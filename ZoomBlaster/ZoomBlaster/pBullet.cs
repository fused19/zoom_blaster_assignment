using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class pBullet
    {
        Texture2D bulletTexture;
        Rectangle bulletRectangle;
        SoundEffect laser;
        bool fire = false;
        bool active = false;
        Rectangle fireButton = new Rectangle(380, 0, 100, 100);

        public pBullet(Texture2D texture, Rectangle rectangle, SoundEffect sound)
        {
            this.bulletTexture = texture;
            this.bulletRectangle = rectangle;
            this.laser = sound;
        }

        public Rectangle pBulRect()
        {
            return this.bulletRectangle;
        }

        public bool GetActive()
        {
            return this.active;
        }
        public bool GetFire()
        {
            return this.fire;
        }

        public void Update(GameTime gameTime, int x, int y)
        {
            foreach (TouchLocation touchFire in TouchPanel.GetState())
            {
                if (fireButton.Contains((int)touchFire.Position.X, (int)touchFire.Position.Y))
                {
                    fire = true;
                    active = true;
                }
            }

            if (fire && active && this.bulletRectangle.Y > -10)
            {
                this.bulletRectangle.Y -= 30;
            }
            else
            {
                this.bulletRectangle.Y = y;
                this.bulletRectangle.X = x + 35;
                fire = false;
                active = false;
            }
            if (this.bulletRectangle.Y == 500)
            {
                laser.Play();
            }
        }
        public void Draw(SpriteBatch bulletDraw)
        {
            if (active)
            {
                bulletDraw.Draw(bulletTexture, bulletRectangle, Color.White);
            }
        }
    }
}