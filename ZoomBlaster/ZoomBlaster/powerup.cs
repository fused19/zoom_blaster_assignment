using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class powerUp
    {
        Texture2D powerUpTexture;
        Rectangle powerUpRectangle;
        Random startPoint = new Random();
        int rndSpeed, rndStart;
        public powerUp()
        {

        }
        public powerUp(Texture2D texture, Rectangle rectangle)
        {
            rndStart = startPoint.Next(40, 390);
            this.powerUpTexture = texture;
            this.powerUpRectangle = rectangle;
            // assign position
            this.powerUpRectangle.Y = -50;
            this.powerUpRectangle.X = rndStart;
        }
        public void Draw(SpriteBatch powerUpDraw)
        {
            powerUpDraw.Draw(powerUpTexture, powerUpRectangle, Color.White);
        }
        public void movement(GameTime gameTime)
        {
            rndSpeed = startPoint.Next(1, 8);
            powerUpRectangle.Y += rndSpeed;
            if (rndSpeed <= 3)
            {
                powerUpRectangle.X += 2;
            }
            else
            {
                powerUpRectangle.X -= 2;
            }
        }
        public Rectangle powerUpRect() { return this.powerUpRectangle; }
        public int xPos() { return this.powerUpRectangle.X; }
        public int yPos() { return this.powerUpRectangle.Y; }
        public void xsetPos(int x) { this.powerUpRectangle.X = x; }
        public void ysetPos(int y) { this.powerUpRectangle.Y = y; }
    }
    public class powerUpList : powerUp
    {
        public List<powerUp> powerUpListing;
        public powerUpList()
            : base()
        {

        }
    }
}