using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class enemy
    {
        Texture2D enemyTexture;
        Rectangle enemyRectangle;
        Random startPoint = new Random();
        int rndSpeed, rndStart;
        public enemy()
        {

        }
        public enemy(Texture2D texture, Rectangle rectangle)
        {
            rndStart = startPoint.Next(40, 390);
            this.enemyTexture = texture;
            this.enemyRectangle = rectangle;
            // assign position
            this.enemyRectangle.Y = -50;
            this.enemyRectangle.X = rndStart;
        }
        public void Draw(SpriteBatch enemyDraw)
        {
            enemyDraw.Draw(enemyTexture, enemyRectangle, Color.White);
        }
        public void movement(GameTime gameTime)
        {
            rndSpeed = startPoint.Next(1, 8);
            enemyRectangle.Y += rndSpeed;
            if (rndSpeed <= 3)
            {
                enemyRectangle.X += 2;
            }
            else
            {
                enemyRectangle.X -= 2;
            }
        }
        public Rectangle enemyRect() { return this.enemyRectangle; }
        public int xPos() { return this.enemyRectangle.X; }
        public int yPos() { return this.enemyRectangle.Y; }
        public void xsetPos(int x) { this.enemyRectangle.X = x; }
        public void ysetPos(int y) { this.enemyRectangle.Y = y; }
    }
    public class enemyList : enemy
    {
        public List<enemy> enemyListing;
        public enemyList()
            : base()
        {

        }
    }
}