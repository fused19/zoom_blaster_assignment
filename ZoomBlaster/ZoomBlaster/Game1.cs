using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace ZoomBlaster
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        /*
         * sound effects produced by:
         * http://www.mobilefish.com/services/sound_effects/sound_effects.php
         */
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        enum GameState
        {
            titleScreen, playingGame, pauseGame, aboutGame, prefsGame, endScreen
        }
        GameState state = GameState.titleScreen;
        SoundEffect theme;
        //hud for game level
        Texture2D hud;
        Rectangle hudRec;
        hub hudDraw;
        // Player
        Texture2D playerImage;
        Rectangle playerRec;
        player hero;
        //bool hit = false;
        // menu background
        Texture2D bntTexture;
        Rectangle bntPlay = new Rectangle(200, 300, 70, 200);
        Rectangle bntAbout = new Rectangle(200, 550, 70, 200);
        Rectangle bntPause = new Rectangle(200, 300, 70, 200);
        Rectangle bntPrefs = new Rectangle(200, 50, 70, 200);
        Rectangle bntExit = new Rectangle(200, 300, 70, 200);
        Rectangle bntMenu = new Rectangle(200, 500, 70, 200);
        Rectangle bntResume = new Rectangle(200, 150, 70, 200);
        Rectangle bntAboutMenu = new Rectangle(330, 310, 70, 200);
        Rectangle bntBack = new Rectangle(300, 300, 70, 200);
        Rectangle bntGameOver = new Rectangle(310, 150, 70, 200);
        Rectangle bntGameOverMenu = new Rectangle(310, 450, 70, 200);
        Texture2D MenuTexture;
        Rectangle MenuRectangle;
        menu MenuBg;
        //pause
        Texture2D PauseTexture;
        Rectangle PauseRectangle;
        pause PauseBg;
        //about
        Texture2D AboutTexture;
        Rectangle AboutRectangle;
        pause AboutBg;
        //prefs
        Texture2D prefsTexture;
        Rectangle prefsRectangle;
        pause prefsBg;
        //game over
        Texture2D gameOverTexture;
        Rectangle gameOverRectangle;
        pause gameOverBg;
        // level one background
        Texture2D bgOne;
        Rectangle scrollRectangle1;
        Rectangle scrollRectangle2;
        screen levelOneBg;
        // pbullet
        Texture2D pBulletImage;
        Rectangle pBulletRect;
        SoundEffect laserSound;
        pBullet pbull;
        // pbullet
        Texture2D aBulletImage;
        Rectangle aBulletRect;
        SoundEffect alaserSound;
        aBullet abull;
        //aliens
        Texture2D alienImage;
        Rectangle alienRec;
        int counter = 4;
        //enemy alien
        Texture2D powerUpImage;
        Rectangle powerUpRec;
        /*
        http://www.gamedev.net/topic/452387-countdown-in-xna/
        */
        List<enemy> EnemyList = new List<enemy>();
        float EnemyCreationTimer = 0;
        List<powerUp> powerUpList = new List<powerUp>();
        float powerUpCreationTimer = 0;
        Random startTime = new Random();
        SpriteFont font;
        Vector2 scorePos;
        score textScoring;
        Vector2 livesPos;
        lives textlives;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = true;
            this.graphics.PreferredBackBufferWidth = 480;
            this.graphics.PreferredBackBufferHeight = 800;
            Content.RootDirectory = "Content";

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);
        }

        /*
         * Update states for the different screens
         * 
         */

        private void upDateTitleScreen(GameTime gameTime)
        {
            MenuBg.Update(gameTime);
            TouchCollection myTouch = TouchPanel.GetState();
            foreach (TouchLocation tap in myTouch)
            {
                TouchLocation oldTap;
                bool oldTapAvailable = tap.TryGetPreviousLocation(out oldTap);
                if (tap.State == TouchLocationState.Pressed && oldTap.State != TouchLocationState.Pressed)
                {
                    if (bntPlay.Contains((int)tap.Position.X, (int)tap.Position.Y))
                    {
                        state = GameState.playingGame;
                    }
                    if (bntAbout.Contains((int)tap.Position.X, (int)tap.Position.Y))
                    {
                        state = GameState.aboutGame;
                    }
                    if (bntPrefs.Contains((int)tap.Position.X, (int)tap.Position.Y))
                    {
                        state = GameState.prefsGame;
                    }
                }
            }
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }
        }
        private void upDatePlayingScreen(GameTime gameTime)
        {
            // player
            hero.Update(gameTime);
            // level one background
            levelOneBg.Update(gameTime);
            //pbullet
            pbull.Update(gameTime, hero.xPos(), hero.yPos());
            //enemy
            EnemyCreationTimer += gameTime.TotalGameTime.Seconds;
            int rndTime = startTime.Next(1, 2);
            if (EnemyCreationTimer >= rndTime && EnemyList.Count <= counter)
            {
                enemy newEnemy = new enemy(alienImage, alienRec);
                EnemyList.Add(newEnemy);
                EnemyCreationTimer = 0;
            }
            foreach (enemy Enemy in EnemyList)
            {
                Enemy.movement(gameTime);
                abull.Update(gameTime, Enemy.xPos(), Enemy.yPos());
                if (Enemy.xPos() <= 0)
                {
                    Enemy.xsetPos(0);
                }
                else if (Enemy.xPos() >= 390)
                {
                    Enemy.xsetPos(Enemy.xPos() - 10);
                }
            }
            for (int i = 0; i < EnemyList.Count; i++)
            {
                if (EnemyList[i].yPos() > 850)
                {
                    EnemyList.RemoveAt(i);
                    --i;
                }
            }
            for (int i = 0; i < EnemyList.Count; i++)
            {
                if (EnemyList[i].enemyRect().Intersects(pbull.pBulRect()) && pbull.GetActive() == true)
                {
                    EnemyList.RemoveAt(i);
                    --i;
                    textScoring.addScore(10);
                }
                else if (EnemyList[i].enemyRect().Intersects(hero.playerRect()))
                {
                    EnemyList.RemoveAt(i);
                    --i;
                    textScoring.addScore(-20);
                    textlives.setLives(-1);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
            powerUpCreationTimer += gameTime.TotalGameTime.Seconds;
            int rndPTime = startTime.Next(1, 2);
            if (powerUpCreationTimer >= rndPTime && powerUpList.Count < 1)
            {
                powerUp newpowerUp = new powerUp(powerUpImage, powerUpRec);
                powerUpList.Add(newpowerUp);
                powerUpCreationTimer = 0;
            }
            foreach (powerUp Power in powerUpList)
            {
                Power.movement(gameTime);
                if (Power.xPos() <= 0)
                {
                    Power.xsetPos(0);
                }
                else if (Power.xPos() >= 390)
                {
                    Power.xsetPos(Power.xPos() - 10);
                }
            }
            for (int i = 0; i < powerUpList.Count; i++)
            {
                if (powerUpList[i].yPos() > 850)
                {
                    powerUpList.RemoveAt(i);
                    --i;
                }
            }
            for (int i = 0; i < powerUpList.Count; i++)
            {
                if (powerUpList[i].powerUpRect().Intersects(hero.playerRect()))
                {
                    powerUpList.RemoveAt(i);
                    --i;
                    textScoring.addScore(20);
                    textlives.setLives(+1);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////
            if (abull.aBulRect().Intersects(hero.playerRect()))
            {
                abull.setxPos(-10);
                abull.setyPos(-10);
                textScoring.addScore(-5);
                textlives.setLives(-1);
            }
            textScoring.Update(gameTime);
            textlives.Update(gameTime);

            if (textlives.getLives() == 0)
            {
                state = GameState.endScreen;
            }
            if (textScoring.getScore() >= 200)
            {
                counter = 5;
            }
            else if (textScoring.getScore() >= 500)
            {
                counter = 6;
            }
            else if (textScoring.getScore() >= 1000)
            {
                counter = 10;
            }
            else
            {
                counter = 4;
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                state = GameState.pauseGame;
            }

        }
        private void upDateEndScreen(GameTime gameTime)
        {
            gameOverBg.Update(gameTime);
            TouchCollection myTouchP = TouchPanel.GetState();
            foreach (TouchLocation tapP in myTouchP)
            {
                /*Idea originated here:
                 * http://stackoverflow.com/a/11250549
                 */
                TouchLocation oldTap;
                bool oldTapAvailable = tapP.TryGetPreviousLocation(out oldTap);
                if (tapP.State == TouchLocationState.Pressed && oldTap.State != TouchLocationState.Pressed)
                {

                    if (bntGameOver.Contains((int)tapP.Position.X, (int)tapP.Position.Y))
                    {
                        this.Exit();
                    }
                    if (bntGameOverMenu.Contains((int)tapP.Position.X, (int)tapP.Position.Y))
                    {
                        state = GameState.titleScreen;
                        textlives.setLives(5);
                        textScoring.setScore(0);
                    }
                }
            }
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

        }
        private void upDatePauseScreen(GameTime gameTime)
        {
            PauseBg.Update(gameTime);
            TouchCollection myTouchP = TouchPanel.GetState();
            foreach (TouchLocation tapP in myTouchP)
            {
                /*Idea originated here:
                 * http://stackoverflow.com/a/11250549
                 */
                TouchLocation oldTap;
                bool oldTapAvailable = tapP.TryGetPreviousLocation(out oldTap);
                if (tapP.State == TouchLocationState.Pressed && oldTap.State != TouchLocationState.Pressed)
                {

                    if (bntResume.Contains((int)tapP.Position.X, (int)tapP.Position.Y))
                    {
                        state = GameState.playingGame;
                    }
                    if (bntMenu.Contains((int)tapP.Position.X, (int)tapP.Position.Y))
                    {
                        state = GameState.titleScreen;
                    }
                }
            }
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                state = GameState.titleScreen;
            }

        }
        private void upDatePrefsScreen(GameTime gameTime)
        {
            prefsBg.Update(gameTime);
            TouchCollection myTouchP = TouchPanel.GetState();
            foreach (TouchLocation tapP in myTouchP)
            {
                /*Idea originated here:
                 * http://stackoverflow.com/a/11250549
                 */
                TouchLocation oldTap;
                bool oldTapAvailable = tapP.TryGetPreviousLocation(out oldTap);
                if (tapP.State == TouchLocationState.Pressed && oldTap.State != TouchLocationState.Pressed)
                {

                    if (bntBack.Contains((int)tapP.Position.X, (int)tapP.Position.Y))
                    {
                        state = GameState.titleScreen;
                    }
                }
            }
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                state = GameState.titleScreen;
            }
        }
        private void upDateAboutScreen(GameTime gameTime)
        {
            AboutBg.Update(gameTime);
            TouchCollection myTouchP = TouchPanel.GetState();
            foreach (TouchLocation tapP in myTouchP)
            {
                /*Idea originated here:
                 * http://stackoverflow.com/a/11250549
                 */
                TouchLocation oldTap;
                bool oldTapAvailable = tapP.TryGetPreviousLocation(out oldTap);
                if (tapP.State == TouchLocationState.Pressed && oldTap.State != TouchLocationState.Pressed)
                {

                    if (bntAboutMenu.Contains((int)tapP.Position.X, (int)tapP.Position.Y))
                    {
                        state = GameState.titleScreen;
                    }
                }
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                state = GameState.titleScreen;
            }
        }

        /*
         * Draw states for the different screens
         * 
         */

        private void drawTitleScreen()
        {
            MenuBg.Draw(spriteBatch);
            spriteBatch.Draw(bntTexture, bntPlay, Color.White);
            spriteBatch.Draw(bntTexture, bntAbout, Color.White);
            spriteBatch.Draw(bntTexture, bntPrefs, Color.White);
        }
        private void drawPlayingScreen()
        {
            //level one background
            levelOneBg.Draw(spriteBatch);
            //player
            hero.Draw(spriteBatch);
            //pBullet
            pbull.Draw(spriteBatch);
            // enemies
            foreach (enemy Enemy in EnemyList)
            {
                Enemy.Draw(spriteBatch);
                //abullet
                abull.Draw(spriteBatch);
            }
            foreach (powerUp Power in powerUpList)
            {
                Power.Draw(spriteBatch);
            }
            // hud
            hudDraw.Draw(spriteBatch);
            // score
            textScoring.Draw(spriteBatch);
            //lives
            textlives.Draw(spriteBatch);
        }
        private void drawEndScreen()
        {
            gameOverBg.Draw(spriteBatch);
            spriteBatch.Draw(bntTexture, bntGameOver, Color.White);
            spriteBatch.Draw(bntTexture, bntGameOverMenu, Color.White);
        }
        private void drawPauseScreen()
        {
            PauseBg.Draw(spriteBatch);
            spriteBatch.Draw(bntTexture, bntMenu, Color.White);
            spriteBatch.Draw(bntTexture, bntResume, Color.White);
        }
        private void drawPrefsScreen()
        {
            prefsBg.Draw(spriteBatch);
            spriteBatch.Draw(bntTexture, bntBack, Color.White);
        }
        private void drawAboutScreen()
        {
            AboutBg.Draw(spriteBatch);
            spriteBatch.Draw(bntTexture, bntAboutMenu, Color.White);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            /*
            http://social.msdn.microsoft.com/Forums/en-US/wpdevelop/thread/95045111-aa60-4c57-85d5-034fd1fbe42e/
            */
            Texture2D splashScreen = Content.Load<Texture2D>(@"textures/splash");
            spriteBatch.Begin();
            spriteBatch.Draw(splashScreen, new Vector2(0, 0), Color.White);
            spriteBatch.End();
            GraphicsDevice.Present();
            //Normal loading of other resources continues here
            theme = Content.Load<SoundEffect>(@"music/theme");
            SoundEffectInstance instance = theme.CreateInstance();
            instance.IsLooped = true;
            instance.Play();
            //menu
            bntTexture = Content.Load<Texture2D>("textures/opacity");
            MenuTexture = Content.Load<Texture2D>(@"textures/menu");
            MenuRectangle = new Rectangle(0, 0, 480, 800);
            MenuBg = new menu(MenuTexture, MenuRectangle);
            //pause
            PauseTexture = Content.Load<Texture2D>(@"textures/pause");
            PauseRectangle = new Rectangle(0, 0, 480, 800);
            PauseBg = new pause(PauseTexture, PauseRectangle);
            //about
            AboutTexture = Content.Load<Texture2D>(@"textures/about");
            AboutRectangle = new Rectangle(0, 0, 480, 800);
            AboutBg = new pause(AboutTexture, AboutRectangle);
            //prefs
            prefsTexture = Content.Load<Texture2D>(@"textures/prefs");
            prefsRectangle = new Rectangle(0, 0, 480, 800);
            prefsBg = new pause(prefsTexture, prefsRectangle);
            //game over
            gameOverTexture = Content.Load<Texture2D>(@"textures/gameover");
            gameOverRectangle = new Rectangle(0, 0, 480, 800);
            gameOverBg = new pause(gameOverTexture, gameOverRectangle);
            //hud
            hud = Content.Load<Texture2D>(@"textures/hud");
            hudRec = new Rectangle(0, 0, 480, 800);
            hudDraw = new hub(hud, hudRec);
            // player
            playerImage = Content.Load<Texture2D>(@"textures/ship");
            playerRec = new Rectangle(GraphicsDevice.Viewport.Width / 2, 600, playerImage.Width, playerImage.Height);
            hero = new player(playerImage, playerRec);
            // level one background
            bgOne = Content.Load<Texture2D>(@"textures/bg");
            scrollRectangle1 = new Rectangle(0, 0, 480, 800);
            scrollRectangle2 = new Rectangle(0, 800, 480, 800);
            levelOneBg = new screen(bgOne, scrollRectangle1, scrollRectangle2);
            // pbullet
            pBulletImage = Content.Load<Texture2D>(@"textures/pBul");
            laserSound = Content.Load<SoundEffect>(@"music/laser");
            pBulletRect = new Rectangle(0, 0, pBulletImage.Width, pBulletImage.Height);
            pbull = new pBullet(pBulletImage, pBulletRect, laserSound);
            // abullet
            aBulletImage = Content.Load<Texture2D>(@"textures/aBul");
            alaserSound = Content.Load<SoundEffect>(@"music/laser");
            aBulletRect = new Rectangle(0, 0, aBulletImage.Width, aBulletImage.Height);
            abull = new aBullet(aBulletImage, aBulletRect);
            // enemy
            alienImage = Content.Load<Texture2D>(@"textures/enemy1");
            alienRec = new Rectangle(0, 800, alienImage.Width, alienImage.Height);
            //alien = new enemy(alienImage, alienRec);
            font = Content.Load<SpriteFont>("gameFont");
            scorePos = new Vector2(100, 100);
            textScoring = new score(scorePos, font);
            livesPos = new Vector2(100, 100);
            textlives = new lives(livesPos, font);
            // powerup
            powerUpImage = Content.Load<Texture2D>(@"textures/powerup");
            powerUpRec = new Rectangle(0, 800, powerUpImage.Width, powerUpImage.Height);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit


            // TODO: Add your update logic here
            switch (state)
            {
                case GameState.titleScreen:
                    upDateTitleScreen(gameTime);
                    break;
                case GameState.playingGame:
                    upDatePlayingScreen(gameTime);
                    break;
                case GameState.pauseGame:
                    upDatePauseScreen(gameTime);
                    break;
                case GameState.prefsGame:
                    upDatePrefsScreen(gameTime);
                    break;
                case GameState.aboutGame:
                    upDateAboutScreen(gameTime);
                    break;
                case GameState.endScreen:
                    upDateEndScreen(gameTime);
                    break;
            }
            Debug.WriteLine(state);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            switch (state)
            {
                case GameState.titleScreen:
                    drawTitleScreen();
                    break;
                case GameState.playingGame:
                    drawPlayingScreen();
                    break;
                case GameState.pauseGame:
                    drawPauseScreen();
                    break;
                case GameState.aboutGame:
                    drawAboutScreen();
                    break;
                case GameState.prefsGame:
                    drawPrefsScreen();
                    break;
                case GameState.endScreen:
                    drawEndScreen();
                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
