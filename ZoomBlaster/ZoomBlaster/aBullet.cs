using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class aBullet
    {
        Texture2D abulletTexture;
        Rectangle abulletRectangle;

        public aBullet(Texture2D texture, Rectangle rectangle)
        {
            this.abulletTexture = texture;
            this.abulletRectangle = rectangle;
        }

        public Rectangle aBulRect()
        {
            return this.abulletRectangle;
        }

        public void Update(GameTime gameTime, int x, int y)
        {
            if (this.abulletRectangle.Y > 850)
            {
                this.abulletRectangle.X = x + 40;
                this.abulletRectangle.Y = y;
            }
            else if (this.abulletRectangle.X <= 0)
            {
                this.abulletRectangle.X = x + 40;
            }
            else
            {
                this.abulletRectangle.Y += 2;
            }

        }
        public void Draw(SpriteBatch abulletDraw)
        {
            abulletDraw.Draw(abulletTexture, abulletRectangle, Color.White);
        }
        public int xPos() { return this.abulletRectangle.X; }
        public int yPos() { return this.abulletRectangle.Y; }
        public void setyPos(int y) { this.abulletRectangle.Y = y; }
        public void setxPos(int x) { this.abulletRectangle.X = x; }
    }
}