using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class screen
    {
        Texture2D levelOneTexture;
        Rectangle levelOneRectangle1;
        Rectangle levelOneRectangle2;


        public screen(Texture2D texture, Rectangle rectangle1, Rectangle rectangle2)
        {
            this.levelOneTexture = texture;
            this.levelOneRectangle1 = rectangle1;
            this.levelOneRectangle2 = rectangle2;
        }
        public screen(Texture2D texture, Rectangle rectangle1)
        {
            this.levelOneTexture = texture;
            this.levelOneRectangle1 = rectangle1;
        }
        public screen()
        {

        }
        public virtual void Update(GameTime gameTime)
        {
            if (levelOneRectangle1.Y + levelOneTexture.Height <= 0)
            {
                levelOneRectangle1.Y = levelOneRectangle2.Y + levelOneTexture.Height;
            }

            if (levelOneRectangle2.Y + levelOneTexture.Height <= 0)
            {
                levelOneRectangle2.Y = levelOneRectangle1.Y + levelOneTexture.Height;
            }

            levelOneRectangle1.Y -= 5;
            levelOneRectangle2.Y -= 5;
        }
        public virtual void Draw(SpriteBatch screenDraw)
        {
            screenDraw.Draw(levelOneTexture, levelOneRectangle1, Color.White);
            screenDraw.Draw(levelOneTexture, levelOneRectangle2, Color.White);
        }
    }
    public class hub : screen
    {
        Texture2D hubTexture;
        Rectangle hubRectangle;
        public hub()
            : base()
        {

        }
        public hub(Texture2D texture, Rectangle rectangle)
        {
            hubTexture = texture;
            hubRectangle = rectangle;
        }

        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(SpriteBatch hubDraw)
        {
            hubDraw.Draw(hubTexture, hubRectangle, Color.White);
        }
    }
    public class menu : screen
    {
        Texture2D menuTexture;
        Rectangle menuRectangle;
        public menu()
            : base()
        {

        }
        public menu(Texture2D texture, Rectangle rectangle)
        {
            menuTexture = texture;
            menuRectangle = rectangle;
        }

        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(SpriteBatch menuDraw)
        {
            menuDraw.Draw(menuTexture, menuRectangle, Color.White);
        }
    }
    public class pause : screen
    {
        Texture2D pauseTexture;
        Rectangle pauseRectangle;
        public pause()
            : base()
        {

        }
        public pause(Texture2D texture, Rectangle rectangle)
        {
            pauseTexture = texture;
            pauseRectangle = rectangle;
        }

        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(SpriteBatch pauseDraw)
        {
            pauseDraw.Draw(pauseTexture, pauseRectangle, Color.White);
        }
    }
    public class about : screen
    {
        Texture2D aboutTexture;
        Rectangle aboutRectangle;
        public about()
            : base()
        {

        }
        public about(Texture2D texture, Rectangle rectangle)
        {
            aboutTexture = texture;
            aboutRectangle = rectangle;
        }

        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(SpriteBatch aboutDraw)
        {
            aboutDraw.Draw(aboutTexture, aboutRectangle, Color.White);
        }
    }
    public class prefs : screen
    {
        Texture2D prefsTexture;
        Rectangle prefsRectangle;
        public prefs()
            : base()
        {

        }
        public prefs(Texture2D texture, Rectangle rectangle)
        {
            prefsTexture = texture;
            prefsRectangle = rectangle;
        }

        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(SpriteBatch prefsDraw)
        {
            prefsDraw.Draw(prefsTexture, prefsRectangle, Color.White);
        }
    }
    public class gameOver : screen
    {
        Texture2D prefsTexture;
        Rectangle prefsRectangle;
        public gameOver()
            : base()
        {

        }
        public gameOver(Texture2D texture, Rectangle rectangle)
        {
            prefsTexture = texture;
            prefsRectangle = rectangle;
        }

        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(SpriteBatch gameOverDraw)
        {
            gameOverDraw.Draw(prefsTexture, prefsRectangle, Color.White);
        }
    }
}