using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class player
    {
        Texture2D playerTexture;
        Rectangle playerRectangle;
        float setPosition = 40f;
        float speed = 10f;

        public player(Texture2D texture, Rectangle rectangle)
        {
            this.playerTexture = texture;
            this.playerRectangle = rectangle;
        }
        public void Update(GameTime gameTime)
        {
            TouchCollection touches = TouchPanel.GetState();

            foreach (TouchLocation touch in touches)
            {
                if (touch.Position.Y > 700)
                {
                    if (touch.Position.X > 480 / 2)
                    {
                        setPosition = setPosition + speed;
                    }
                    else
                    {
                        setPosition = setPosition - speed;
                    }
                    if (setPosition < this.playerTexture.Width / 2)
                    {
                        setPosition = this.playerTexture.Width / 2;
                    }
                    else if (setPosition > 500 - (this.playerTexture.Width * 2))
                    {
                        setPosition = 500 - (this.playerTexture.Width * 2);
                    }
                }

            }
            this.playerRectangle.X = (int)(setPosition + speed);
        }
        public void Draw(SpriteBatch playerDraw)
        {
            playerDraw.Draw(playerTexture, playerRectangle, Color.White);
        }
        public int xPos() { return this.playerRectangle.X; }
        public int yPos() { return this.playerRectangle.Y; }
        public Rectangle playerRect()
        {
            return this.playerRectangle;
        }
    }
}