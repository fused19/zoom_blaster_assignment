using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace ZoomBlaster
{
    public class score
    {
        SpriteFont font;
        int _score = 0;
        string scoreText;
        Vector2 textRect;
        double rot = -1.57;

        public score(Vector2 rectangle, SpriteFont text)
        {
            textRect = rectangle;
            font = text;
        }
        public score()
        {

        }
        public void setScore(int s)
        {
            _score = s;
        }
        public int getScore()
        {
            return _score;
        }
        public void addScore(int s)
        {
            _score += s;
        }
        public string textformat()
        {
            if (_score <= 0)
            {
                scoreText = "Score: minus ";
            }

            else if (_score > 0)
            {
                scoreText = "Score: plus ";

            }
            return scoreText;
        }
        public virtual void Update(GameTime gameTime)
        {
            this.textformat();
        }
        public virtual void Draw(SpriteBatch fontDraw)
        {
            Vector2 position = new Vector2(20, 250);  // Assumes position already declared
            Vector2 origin = new Vector2(0, 0);
            fontDraw.DrawString(font, textformat() + _score.ToString(), position, Color.White, (float)rot,
                origin, 1.0f, SpriteEffects.None, 1.0f);
        }
    }
    public class lives : score
    {
        SpriteFont font;
        int _lives = 5;
        string livesText;
        Vector2 livesRect;
        double rot = -1.57;
        public lives()
            : base()
        {

        }
        public lives(Vector2 rectangle, SpriteFont text)
        {
            livesRect = rectangle;
            font = text;
        }
        public void setLives(int l)
        {
            _lives += l;
        }
        public int getLives()
        {
            return _lives;
        }
        public string textLivesFormat()
        {
            if (_lives <= 0)
            {
                livesText = "Lives: -minus ";
            }

            else if (_lives > 0)
            {
                livesText = "Lives: plus ";

            }
            return livesText;
        }
        public override void Update(GameTime gameTime)
        {
            this.textLivesFormat();
        }
        public override void Draw(SpriteBatch fontDraw)
        {
            Vector2 position2 = new Vector2(20, 500);  // Assumes position already declared
            Vector2 origin2 = new Vector2(0, 0);
            fontDraw.DrawString(font, textLivesFormat() + _lives.ToString(), position2, Color.White, (float)rot,
                origin2, 1.0f, SpriteEffects.None, 1.0f);
        }
    }

}